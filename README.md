# Dz02_01

## Opis

Potrebno je implementirati interfejs `IHelloWorld`.  
Potrebno je da metoda `helloWorld()` vraća string "Hello World".

```java
public interface IHelloWorld {
    public String helloWorld();
}
```

## Ciljevi vežbe
* Priprema za rad sa domaćim zadacima
* Uspešno skidanje koda sa gitlab-a
* Izvršavanje maven komandi

## Testiranje
Nako što ispišete kod možete proveriti da li kod uspešno radi
Jedan od načina je da u NetBeans-u, u Project view-u u projektu otvorite Test Packages, rađirite paket i kliknite desnim tasterom na klasu Test.java a zatim izaberite "Test file".  
Na ovaj način ćete pokrenuti testove.
